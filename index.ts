import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as mime from "mime";
import * as fs from "fs";
//import * as path from "path";
import { execSync } from 'child_process';

execSync("npm --prefix ./vue-boilerplate run build");

const bucket = new aws.s3.Bucket("pulumiBucket", {
    acl: "public-read",
    //policy: fs.readFileSync("./policy.json").toString(),
    website: {
        indexDocument: "index.html"
    }
});

let siteDir = "./vue-boilerplate/dist";
let assets = walk(siteDir);

for(let asset of assets){
    let file = asset.replace(siteDir + "/", "");

    new aws.s3.BucketObject(file, {
        bucket: bucket,
        source: new pulumi.asset.FileAsset(asset),
        contentType: mime.getType(file) || undefined,
    });
}

new aws.s3.BucketPolicy("bucketPolicy", {
    bucket: bucket.bucket,
    policy: bucket.bucket.apply(publicReadPolicyForBucket)
});

function publicReadPolicyForBucket(bucketName:string) {
    return JSON.stringify({
        Version: "2012-10-17",
        Statement: [{
            Effect: "Allow",
            Principal: "*",
            Action: [
                "s3:GetObject"
            ],
            Resource: [
                `arn:aws:s3:::${bucketName}/*`
            ]
        }]
    })
}

function walk(dir: string) {
    var results:string[] = [];
    var list = fs.readdirSync(dir);

    list.forEach(function(file) {
        file = dir + '/' + file;
        var stat = fs.statSync(file);
        
        if (stat && stat.isDirectory()) { 
            /* Recurse into a subdirectory */
            //results.push(file);
            results = results.concat(walk(file));
        } else { 
            /* Is a file */
            results.push(file);
        }
    });
    return results;
}

/*
for (let item of fs.readdirSync(siteDir)) {
    let filePath = path.join(siteDir, item);
    
    if(fs.lstatSync(filePath).isDirectory()){        
        new aws.s3.BucketObject(item, {
            bucket: bucket,
            source: new pulumi.asset.FileArchive(filePath),
            //contentType: mime.getType(filePath) || undefined,
        });
    }
    else{
        new aws.s3.BucketObject(item, {
            bucket: bucket,
            source: new pulumi.asset.FileAsset(filePath),
            contentType: mime.getType(filePath) || undefined,
        });
    }
}
*/

exports.testURL = bucket.websiteEndpoint;